Panel de Control
================

Se trata de un una panel para poder usarlo en simuladores de vuelo. El objetivo
principal de este proyecto ha sido aprender sobre microcontroladores de 32 bits,
en contcreto, los ARM Cortex-M e introducirse en los RTOS.

Todo lo utilizado en el proyecto se encuentra en este repositorio: firmware, planos, serigrafía, etc...

![Panel_de_control](https://4.bp.blogspot.com/-y6eMzSS5x2U/WyQI-vBe_nI/AAAAAAAAAKk/XAp4zXetjY8uhpVo_EdwiDR7Lhnnd9EjgCKgBGAs/s1600/20180221_0003.jpgmage.png)

Las características del panel son las siguientes:

- 26 Botones.
- 40 Interruptores de 2 posiciones.
- 21 Interruptores de 3 posiciones.
- 4 Rotatorios.
- 2 Encoders.
- 1 Minijoystick.
- 2 MFDs de 20 botones cada uno.
- 1 Panel LED LCD de 15.1" a modo de monitor.
- Una sola conexión al PC mediante USB.
- Un solo cable de alimentación.
- Posibilidad de usar solamente la parte queincluye el monitor o añadirle unaexpansión con más interruptores, inclusomientras está en funcionamiento.

Firmware:

- Comunicación con control de errores (CRC 16).
- Protección contra desbordamientos de la pila.
- Reinicio automático en caso de fallo. (Watchdog).
- USB modo HID en RAW. (para no utilizar drivers).

Para más información consultar el indice del proyecto en el [blog.](https://www.circuiteando.net/2018/05/panel-de-control-resumen-e-indice.html)