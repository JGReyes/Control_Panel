/*
 * main.c
 *
 *  Created on: 05/10/2016
 *      Author: JGReyes (www.circuiteando.net)
 *
 *  License: GPLv3 (www.gnu.org/licenses/gpl.html)
 */

#include <stdio.h>
#include "ch.h"
#include "hal.h"
#include "button_debounce.h"
#include "chprintf.h"
#include "quadrature_encoder.h"
#include "main.h"
#include "lib_crc.h"

/*  Constantes
 *
 */

#define n_canales  6
#define n_muestras 10

/**
 * Estructura para la configuración del ADC
 */
const ADCConversionGroup grupo_conversion = {
    FALSE, /*NOT CIRCULAR*/
    n_canales, /*NUMB OF CH*/
    NULL, /*NO ADC CALLBACK*/
    NULL, /*NO ADC ERROR CALLBACK*/
    0, /* CR1 */
    ADC_CR2_SWSTART, /* CR2 */
    0, /* SMPR1 */
    ADC_SMPR2_SMP_AN10(ADC_SAMPLE_192) | /* SMPR2 */
    ADC_SMPR2_SMP_AN11(ADC_SAMPLE_192) |
    ADC_SMPR2_SMP_AN12(ADC_SAMPLE_192) |
    ADC_SMPR2_SMP_AN13(ADC_SAMPLE_192),
    ADC_SMPR3_SMP_AN0(ADC_SAMPLE_192) | /* SMPR3 */
    ADC_SMPR3_SMP_AN1(ADC_SAMPLE_192),
    ADC_SQR1_NUM_CH(n_canales), /* SQR1 */
    0, /* SQR2 */
    0, /* SQR3 */
    0, /* SQR4 */
    ADC_SQR5_SQ1_N(ADC_CHANNEL_IN0) | /* SQR5 */
    ADC_SQR5_SQ2_N(ADC_CHANNEL_IN1) | ADC_SQR5_SQ3_N(ADC_CHANNEL_IN10)
        | ADC_SQR5_SQ4_N(ADC_CHANNEL_IN11) | ADC_SQR5_SQ5_N(ADC_CHANNEL_IN12)
        | ADC_SQR5_SQ6_N(ADC_CHANNEL_IN13)

};

/* Mutex para que los hilos no compitan por mandar teclas */
static mutex_t mtx_tecla;
thread_t *tp = NULL;
thread_t *tp_rx = NULL;

/* Buffers para comunicaciones UART   */
uint8_t trama[6] = {0, 0, 0, 0, 0, 0};
uint8_t rx_buffer_1[6] = {0, 0, 0, 0, 0, 0};
uint8_t rx_buffer_2[6] = {0, 0, 0, 0, 0, 0};
uint8_t rx_buffer_3[6] = {0, 0, 0, 0, 0, 0};

/* Contadores para los encoders */
int32_t contador_enc1 = 0;
int32_t contador_enc2 = 0;

/* Configuración del watchdog  */
WDGConfig wdgConf = {2,    // Prescaler. /16
    0xfff // Valor de recarga del contador. 1,77 segundos aprox.
    };

/*
 * Configuración de los 3 UARTs
 */
static UARTConfig uart_cfg_1 = {
  NULL,
  NULL,
  rxend1,
  NULL,
  NULL,
  115200,
  0,
  USART_CR2_LINEN,
  0
};

static UARTConfig uart_cfg_2 = {
  NULL,
  NULL,
  rxend2,
  NULL,
  NULL,
  57600,
  0,
  USART_CR2_LINEN,
  0
};

static UARTConfig uart_cfg_3 = {
  NULL,
  NULL,
  rxend3,
  NULL,
  NULL,
  115200,
  0,
  USART_CR2_LINEN,
  0
};

/* Serial stream
 *
 */

//BaseSequentialStream *chp = (BaseSequentialStream*)&SD2;

/* Working areas for threads
 *
 */

static THD_WORKING_AREA(waLeer_Nucleo_0, 512);
static THD_WORKING_AREA(waLeer_Analog, 512);
static THD_WORKING_AREA(waLeer_Encoders, 512);
static THD_WORKING_AREA(waRx_Serial, 512);

/* Main
 *
 */
int main(void) {

  halInit();
  chSysInit();

  wdgStart(&WDGD1, &wdgConf);

  chMtxObjectInit(&mtx_tecla);

  uartStart(&UARTD1, &uart_cfg_1);
  uartStart(&UARTD2, &uart_cfg_2);
  uartStart(&UARTD3, &uart_cfg_3);
 // sdStart(&SD2, NULL);
  adcStart(&ADCD1, NULL);
  initExtEnc();

  chThdCreateStatic(waRx_Serial, sizeof(waRx_Serial), NORMALPRIO + 1, rx_serial,
                      NULL);

  chThdCreateStatic(waLeer_Nucleo_0, sizeof(waLeer_Nucleo_0), NORMALPRIO + 1,
                    Leer_Nucleo_0, NULL);

  chThdCreateStatic(waLeer_Analog, sizeof(waLeer_Analog), NORMALPRIO + 1,
                    Leer_Analog, NULL);

  chThdCreateStatic(waLeer_Encoders, sizeof(waLeer_Encoders), NORMALPRIO + 2,
                    Leer_Encoders, NULL);


  chThdSleepMilliseconds(1000);
  uartStartReceive(&UARTD1, 6, &rx_buffer_1);
  uartStartReceive(&UARTD2, 6, &rx_buffer_2);
  uartStartReceive(&UARTD3, 6, &rx_buffer_3);

  while (true) {
    chThdSleepMilliseconds(5000);
  }
}

/**
 * Cambia un bit especifico en una variable de 8 bits.
 *
 * @param variable  Variable que se quiere modificar.
 * @param posicion  Posición del bit 0..7.
 * @param valor     Valor que tendrá el bit true (1) o false (0)
 */
void configBit(uint8_t *variable, uint8_t posicion, bool valor) {

  if (valor) {
    *variable |= (1 << posicion);

  }
  else {
    *variable &= ~(1 << posicion);
  }

}

/**
 * Manda por puerto serie el comando asociado a la tecla pulsada.
 *
 * @param interruptor Número uint32 de la tecla pulsada.
 */
void mandar_tecla(uint32_t interruptor) {

  static bool mandar = false;
  size_t tamano = 6;

  chMtxLock(&mtx_tecla);

  // Previene falsas teclas mientras se inicializan las rutinas antirrebotes.
    if (mandar == false){
      uint32_t now = ST2S(chVTGetSystemTime());
      if (now > 2)
        mandar = true;
    }

   // Manda la tecla una vez pasado el tiempo inicial de 2 segundos.
   if (mandar){
     prepara_trama(trama, interruptor);
     uartAcquireBus(&UARTD2);
     uartSendFullTimeout(&UARTD2, &tamano, &trama, MS2ST(25));
     uartReleaseBus(&UARTD2);
   }

  chMtxUnlock(&mtx_tecla);
}

/**
 * Función encargada de prepara una trama para enviar un comando.
 * @param trama_buffer  Buffer que contendrá la trama a enviar.
 * @param comando       Comando que se quiere enviar.
 */
void prepara_trama(uint8_t *trama_buffer, uint32_t comando){
    uint8_t cmd[3] = {0, 0, 0};
    cmd[0] = comando;
    cmd[1] = (comando >> 8);
    cmd[2] = (comando >> 16);

    uint16_t crc = calculate_crc16(cmd, 3);

    trama_buffer[0] = 0x7e;            // Byte de sincronización
    trama_buffer[1] = cmd[2];          // Interruptor MSB primero 3 Bytes
    trama_buffer[2] = cmd[1];
    trama_buffer[3] = cmd[0];
    trama_buffer[4] = (crc >> 8);     // Crc MSB
    trama_buffer[5] = crc;            // Crc LSB

}

/**
 * Función encargada de recuperar el comando dentro de la trama.
 * @param trama_buffer  Buffer que contiene la trama a leer.
 * @return              Comando de la trama, 0 en caso de error.
 */
uint32_t leer_trama(const uint8_t *trama_buffer){
  if (trama_buffer[0] == 0x7e) {

       uint32_t comando = (trama_buffer[1] << 16) | (trama_buffer[2] << 8)
           | (trama_buffer[3]);

       uint16_t crc = (trama_buffer[4] << 8) | (trama_buffer[5]);

       uint8_t cmd[3] = {0, 0, 0};
       cmd[0] = comando;
       cmd[1] = (comando >> 8);
       cmd[2] = (comando >> 16);

       if (calculate_crc16(cmd, 3) == crc) {
         /* Trama válida */
         return comando;
       }else{
         /*Crc erróneo */
         return 0;
       }
  }else{
    /* Sin byte de sincronización, trama nó válida*/
    return 0;
  }
}

/**
 * Es un hilo de ejecución que lee las entradas y manda los comandos correspondientes.
 *
 * Los comandos están formados por 5 números en hexadecimal que indican por orden:
 * N de placa nucleo, Puerto, Debouncer, Botón, y Estado del mismo.
 * Ej: 0x3A120  Indica que el botón o interruptor está en la placa 3, en el puerto A,
 * que el Debouncer 1 se encarga de eliminar los rebotes, que dentro del Debouncer 1 es el
 * botón 2 y que el botón está a 0 lógico (pulsado, ya que está en pullup).
 */
static THD_FUNCTION(Leer_Nucleo_0, arg) {

  chRegSetThreadName("Leer_Nucleo_0");

   uint8_t bloque1 = 0;
   uint8_t bloque2 = 0;
   uint8_t bloque3 = 0;
   uint8_t bloque4 = 0;
   uint8_t bloque5 = 0;
   uint8_t bloque6 = 0;
   uint8_t bloque7 = 0;

   Debouncer dbr_bloque1;
   Debouncer dbr_bloque2;
   Debouncer dbr_bloque3;
   Debouncer dbr_bloque4;
   Debouncer dbr_bloque5;
   Debouncer dbr_bloque6;
   Debouncer dbr_bloque7;

   ButtonDebounceInit(&dbr_bloque1, 0xff);
   ButtonDebounceInit(&dbr_bloque2, 0xff);
   ButtonDebounceInit(&dbr_bloque3, 0xff);
   ButtonDebounceInit(&dbr_bloque4, 0xff);
   ButtonDebounceInit(&dbr_bloque5, 0xff);
   ButtonDebounceInit(&dbr_bloque6, 0xff);
   ButtonDebounceInit(&dbr_bloque7, 0xff);

   while (true) {

     uint16_t porta = palReadGroup(GPIOA, 0x99f0, 0);

     /* Bloque 1 */
     bloque1 = porta;

     /* Bloque 2 */
     bloque2 = (porta >> 8);

     uint16_t portb = palReadGroup(GPIOB, 0xf037, 0);
     /* Bloque 3 */
     bloque3 = portb;

     /* Bloque 4 */
     bloque4 = (portb >> 8);

     uint16_t portc = palReadGroup(GPIOC, 0x3ff0, 0);
     /* Bloque 5 */
     bloque5 = portc;

     /* Bloque 6 */
     bloque6 = (portc >> 8);

     uint16_t portd = palReadGroup(GPIOD, 0x0004, 0);
     /* Bloque 7 */
     bloque7 = portd;

     ButtonProcess(&dbr_bloque1, bloque1);
     ButtonProcess(&dbr_bloque2, bloque2);
     ButtonProcess(&dbr_bloque3, bloque3);
     ButtonProcess(&dbr_bloque4, bloque4);
     ButtonProcess(&dbr_bloque5, bloque5);
     ButtonProcess(&dbr_bloque6, bloque6);
     ButtonProcess(&dbr_bloque7, bloque7);

     /* Bloque 1 Pressed */

     if (ButtonPressed(&dbr_bloque1, BUTTON_PIN_0)) {
       mandar_tecla(0x0A100);

     }

     if (ButtonPressed(&dbr_bloque1, BUTTON_PIN_1)) {
       mandar_tecla(0x0A110);

     }

     if (ButtonPressed(&dbr_bloque1, BUTTON_PIN_2)) {
       mandar_tecla(0x0A120);

     }

     if (ButtonPressed(&dbr_bloque1, BUTTON_PIN_3)) {
       mandar_tecla(0x0A130);

     }

     if (ButtonPressed(&dbr_bloque1, BUTTON_PIN_4)) {
       mandar_tecla(0x0A140);

     }

     if (ButtonPressed(&dbr_bloque1, BUTTON_PIN_5)) {
       mandar_tecla(0x0A150);

     }

     if (ButtonPressed(&dbr_bloque1, BUTTON_PIN_6)) {
       mandar_tecla(0x0A160);

     }

     if (ButtonPressed(&dbr_bloque1, BUTTON_PIN_7)) {
       mandar_tecla(0x0A170);

     }

     /* Bloque 1 Released */

     if (ButtonReleased(&dbr_bloque1, BUTTON_PIN_0)) {
       mandar_tecla(0x0A101);

     }

     if (ButtonReleased(&dbr_bloque1, BUTTON_PIN_1)) {
       mandar_tecla(0x0A111);

     }

     if (ButtonReleased(&dbr_bloque1, BUTTON_PIN_2)) {
       mandar_tecla(0x0A121);

     }

     if (ButtonReleased(&dbr_bloque1, BUTTON_PIN_3)) {
       mandar_tecla(0x0A131);

     }

     if (ButtonReleased(&dbr_bloque1, BUTTON_PIN_4)) {
       mandar_tecla(0x0A141);

     }

     if (ButtonReleased(&dbr_bloque1, BUTTON_PIN_5)) {
       mandar_tecla(0x0A151);

     }

     if (ButtonReleased(&dbr_bloque1, BUTTON_PIN_6)) {
       mandar_tecla(0x0A161);

     }

     if (ButtonReleased(&dbr_bloque1, BUTTON_PIN_7)) {
       mandar_tecla(0x0A171);

     }

     /* Bloque 2 Pressed */

     if (ButtonPressed(&dbr_bloque2, BUTTON_PIN_0)) {
       mandar_tecla(0x0A200);

     }

     if (ButtonPressed(&dbr_bloque2, BUTTON_PIN_1)) {
       mandar_tecla(0x0A210);

     }

     if (ButtonPressed(&dbr_bloque2, BUTTON_PIN_2)) {
       mandar_tecla(0x0A220);

     }

     if (ButtonPressed(&dbr_bloque2, BUTTON_PIN_3)) {
       mandar_tecla(0x0A230);

     }

     if (ButtonPressed(&dbr_bloque2, BUTTON_PIN_4)) {
       mandar_tecla(0x0A240);

     }

     if (ButtonPressed(&dbr_bloque2, BUTTON_PIN_5)) {
       mandar_tecla(0x0A250);

     }

     if (ButtonPressed(&dbr_bloque2, BUTTON_PIN_6)) {
       mandar_tecla(0x0A260);

     }

     if (ButtonPressed(&dbr_bloque2, BUTTON_PIN_7)) {
       mandar_tecla(0x0A270);

     }

     /* Bloque 2 Released */

     if (ButtonReleased(&dbr_bloque2, BUTTON_PIN_0)) {
       mandar_tecla(0x0A201);

     }

     if (ButtonReleased(&dbr_bloque2, BUTTON_PIN_1)) {
       mandar_tecla(0x0A211);

     }

     if (ButtonReleased(&dbr_bloque2, BUTTON_PIN_2)) {
       mandar_tecla(0x0A221);

     }

     if (ButtonReleased(&dbr_bloque2, BUTTON_PIN_3)) {
       mandar_tecla(0x0A231);

     }

     if (ButtonReleased(&dbr_bloque2, BUTTON_PIN_4)) {
       mandar_tecla(0x0A241);

     }

     if (ButtonReleased(&dbr_bloque2, BUTTON_PIN_5)) {
       mandar_tecla(0x0A251);

     }

     if (ButtonReleased(&dbr_bloque2, BUTTON_PIN_6)) {
       mandar_tecla(0x0A261);

     }

     if (ButtonReleased(&dbr_bloque2, BUTTON_PIN_7)) {
       mandar_tecla(0x0A271);

     }

     /* Bloque 3 Pressed */

     if (ButtonPressed(&dbr_bloque3, BUTTON_PIN_0)) {
       mandar_tecla(0x0B300);

     }

     if (ButtonPressed(&dbr_bloque3, BUTTON_PIN_1)) {
       mandar_tecla(0x0B310);

     }

     if (ButtonPressed(&dbr_bloque3, BUTTON_PIN_2)) {
       mandar_tecla(0x0B320);

     }

     if (ButtonPressed(&dbr_bloque3, BUTTON_PIN_3)) {
       mandar_tecla(0x0B330);

     }

     if (ButtonPressed(&dbr_bloque3, BUTTON_PIN_4)) {
       mandar_tecla(0x0B340);

     }

     if (ButtonPressed(&dbr_bloque3, BUTTON_PIN_5)) {
       mandar_tecla(0x0B350);

     }

     if (ButtonPressed(&dbr_bloque3, BUTTON_PIN_6)) {
       mandar_tecla(0x0B360);

     }

     if (ButtonPressed(&dbr_bloque3, BUTTON_PIN_7)) {
       mandar_tecla(0x0B370);

     }

     /* Bloque 3 Released */

     if (ButtonReleased(&dbr_bloque3, BUTTON_PIN_0)) {
       mandar_tecla(0x0B301);

     }

     if (ButtonReleased(&dbr_bloque3, BUTTON_PIN_1)) {
       mandar_tecla(0x0B311);

     }

     if (ButtonReleased(&dbr_bloque3, BUTTON_PIN_2)) {
       mandar_tecla(0x0B321);

     }

     if (ButtonReleased(&dbr_bloque3, BUTTON_PIN_3)) {
       mandar_tecla(0x0B331);

     }

     if (ButtonReleased(&dbr_bloque3, BUTTON_PIN_4)) {
       mandar_tecla(0x0B341);

     }

     if (ButtonReleased(&dbr_bloque3, BUTTON_PIN_5)) {
       mandar_tecla(0x0B351);

     }

     if (ButtonReleased(&dbr_bloque3, BUTTON_PIN_6)) {
       mandar_tecla(0x0B361);

     }

     if (ButtonReleased(&dbr_bloque3, BUTTON_PIN_7)) {
       mandar_tecla(0x0B371);

     }

     /* Bloque 4 Pressed */

     if (ButtonPressed(&dbr_bloque4, BUTTON_PIN_0)) {
       mandar_tecla(0x0B400);

     }

     if (ButtonPressed(&dbr_bloque4, BUTTON_PIN_1)) {
       mandar_tecla(0x0B410);

     }

     if (ButtonPressed(&dbr_bloque4, BUTTON_PIN_2)) {
       mandar_tecla(0x0B420);

     }

     if (ButtonPressed(&dbr_bloque4, BUTTON_PIN_3)) {
       mandar_tecla(0x0B430);

     }

     if (ButtonPressed(&dbr_bloque4, BUTTON_PIN_4)) {
       mandar_tecla(0x0B440);

     }

     if (ButtonPressed(&dbr_bloque4, BUTTON_PIN_5)) {
       mandar_tecla(0x0B450);

     }

     if (ButtonPressed(&dbr_bloque4, BUTTON_PIN_6)) {
       mandar_tecla(0x0B460);

     }

     if (ButtonPressed(&dbr_bloque4, BUTTON_PIN_7)) {
       mandar_tecla(0x0B470);

     }

     /* Bloque 4 Released */

     if (ButtonReleased(&dbr_bloque4, BUTTON_PIN_0)) {
       mandar_tecla(0x0B401);

     }

     if (ButtonReleased(&dbr_bloque4, BUTTON_PIN_1)) {
       mandar_tecla(0x0B411);

     }

     if (ButtonReleased(&dbr_bloque4, BUTTON_PIN_2)) {
       mandar_tecla(0x0B421);

     }

     if (ButtonReleased(&dbr_bloque4, BUTTON_PIN_3)) {
       mandar_tecla(0x0B431);

     }

     if (ButtonReleased(&dbr_bloque4, BUTTON_PIN_4)) {
       mandar_tecla(0x0B441);

     }

     if (ButtonReleased(&dbr_bloque4, BUTTON_PIN_5)) {
       mandar_tecla(0x0B451);

     }

     if (ButtonReleased(&dbr_bloque4, BUTTON_PIN_6)) {
       mandar_tecla(0x0B461);

     }

     if (ButtonReleased(&dbr_bloque4, BUTTON_PIN_7)) {
       mandar_tecla(0x0B471);

     }

     /* Bloque 5 Pressed */

     if (ButtonPressed(&dbr_bloque5, BUTTON_PIN_0)) {
       mandar_tecla(0x0C500);

     }

     if (ButtonPressed(&dbr_bloque5, BUTTON_PIN_1)) {
       mandar_tecla(0x0C510);

     }

     if (ButtonPressed(&dbr_bloque5, BUTTON_PIN_2)) {
       mandar_tecla(0x0C520);

     }

     if (ButtonPressed(&dbr_bloque5, BUTTON_PIN_3)) {
       mandar_tecla(0x0C530);

     }

     if (ButtonPressed(&dbr_bloque5, BUTTON_PIN_4)) {
       mandar_tecla(0x0C540);

     }

     if (ButtonPressed(&dbr_bloque5, BUTTON_PIN_5)) {
       mandar_tecla(0x0C550);

     }

     if (ButtonPressed(&dbr_bloque5, BUTTON_PIN_6)) {
       mandar_tecla(0x0C560);

     }

     if (ButtonPressed(&dbr_bloque5, BUTTON_PIN_7)) {
       mandar_tecla(0x0C570);

     }

     /* Bloque 5 Released */

     if (ButtonReleased(&dbr_bloque5, BUTTON_PIN_0)) {
       mandar_tecla(0x0C501);

     }

     if (ButtonReleased(&dbr_bloque5, BUTTON_PIN_1)) {
       mandar_tecla(0x0C511);

     }

     if (ButtonReleased(&dbr_bloque5, BUTTON_PIN_2)) {
       mandar_tecla(0x0C521);

     }

     if (ButtonReleased(&dbr_bloque5, BUTTON_PIN_3)) {
       mandar_tecla(0x0C531);

     }

     if (ButtonReleased(&dbr_bloque5, BUTTON_PIN_4)) {
       mandar_tecla(0x0C541);

     }

     if (ButtonReleased(&dbr_bloque5, BUTTON_PIN_5)) {
       mandar_tecla(0x0C551);

     }

     if (ButtonReleased(&dbr_bloque5, BUTTON_PIN_6)) {
       mandar_tecla(0x0C561);

     }

     if (ButtonReleased(&dbr_bloque5, BUTTON_PIN_7)) {
       mandar_tecla(0x0C571);

     }

     /* Bloque 6 Pressed */

     if (ButtonPressed(&dbr_bloque6, BUTTON_PIN_0)) {
       mandar_tecla(0x0C600);

     }

     if (ButtonPressed(&dbr_bloque6, BUTTON_PIN_1)) {
       mandar_tecla(0x0C610);

     }

     if (ButtonPressed(&dbr_bloque6, BUTTON_PIN_2)) {
       mandar_tecla(0x0C620);

     }

     if (ButtonPressed(&dbr_bloque6, BUTTON_PIN_3)) {
       mandar_tecla(0x0C630);

     }

     if (ButtonPressed(&dbr_bloque6, BUTTON_PIN_4)) {
       mandar_tecla(0x0C640);

     }

     if (ButtonPressed(&dbr_bloque6, BUTTON_PIN_5)) {
       mandar_tecla(0x0C650);

     }

     if (ButtonPressed(&dbr_bloque6, BUTTON_PIN_6)) {
       mandar_tecla(0x0C660);

     }

     if (ButtonPressed(&dbr_bloque6, BUTTON_PIN_7)) {
       mandar_tecla(0x0C670);

     }

     /* Bloque 6 Released */

     if (ButtonReleased(&dbr_bloque6, BUTTON_PIN_0)) {
       mandar_tecla(0x0C601);

     }

     if (ButtonReleased(&dbr_bloque6, BUTTON_PIN_1)) {
       mandar_tecla(0x0C611);

     }

     if (ButtonReleased(&dbr_bloque6, BUTTON_PIN_2)) {
       mandar_tecla(0x0C621);

     }

     if (ButtonReleased(&dbr_bloque6, BUTTON_PIN_3)) {
       mandar_tecla(0x0C631);

     }

     if (ButtonReleased(&dbr_bloque6, BUTTON_PIN_4)) {
       mandar_tecla(0x0C641);

     }

     if (ButtonReleased(&dbr_bloque6, BUTTON_PIN_5)) {
       mandar_tecla(0x0C651);

     }

     if (ButtonReleased(&dbr_bloque6, BUTTON_PIN_6)) {
       mandar_tecla(0x0C661);

     }

     if (ButtonReleased(&dbr_bloque6, BUTTON_PIN_7)) {
       mandar_tecla(0x0C671);

     }

     /* Bloque 7 Pressed */

     if (ButtonPressed(&dbr_bloque7, BUTTON_PIN_2)) {
       mandar_tecla(0x0D720);

     }

     /* Bloque 7 Released */

     if (ButtonReleased(&dbr_bloque7, BUTTON_PIN_2)) {
       mandar_tecla(0x0D721);

     }

     wdgReset(&WDGD1); // Reset del watchdog.
     chThdSleepMilliseconds(5);

   }
}

/**
 * Hilo encargado de leer las entrada analógicas.
 *
 */
static THD_FUNCTION(Leer_Analog, arg) {

  chRegSetThreadName("Leer_Analog");

  adcsample_t buffer_adc[n_muestras][n_canales];

  uint8_t pos_ant_rot1 = 0;
  uint8_t pos_ant_rot2 = 0;
  uint8_t pos_ant_rot3 = 0;
  uint8_t pos_ant_rot4 = 0;
  uint8_t pos_ant_eje_x = 3;
  uint8_t pos_ant_eje_y = 3;

  uint16_t valor = 0;
  uint8_t i = 0;

  while (true) {

    uint32_t suma_rot1 = 0;
    uint32_t suma_rot2 = 0;
    uint32_t suma_rot3 = 0;
    uint32_t suma_rot4 = 0;
    uint32_t suma_eje_x = 0;
    uint32_t suma_eje_y = 0;

    adcConvert(&ADCD1, &grupo_conversion, (adcsample_t*)buffer_adc,
    n_muestras);

    for (i = 0; i < n_muestras; i++) {

      suma_eje_y += buffer_adc[i][0];
      suma_eje_x += buffer_adc[i][1];
      suma_rot1 += buffer_adc[i][2];
      suma_rot2 += buffer_adc[i][3];
      suma_rot3 += buffer_adc[i][4];
      suma_rot4 += buffer_adc[i][5];

    }

    // rot 1
    valor = (uint16_t)(suma_rot1 / n_muestras);
    valor = map(valor, 0, 4095, 0, 65535);

#ifdef DEBUG_ANALOG
    chprintf(chp, "Valor rot1: %u \r\n", valor);
#endif

    if (valor < 7000) {  // Pos.0

      if (pos_ant_rot1 != 0) {

        pos_ant_rot1 = 0;
        mandar_tecla(0xA0);
      }
    }

    if ((valor > 7000) && (valor < 18000)) {  // Pos.1

      if (pos_ant_rot1 != 1) {

        pos_ant_rot1 = 1;
        mandar_tecla(0xA1);
      }
    }

    if ((valor > 18000) && (valor < 25000)) {  // Pos.2

      if (pos_ant_rot1 != 2) {

        pos_ant_rot1 = 2;
        mandar_tecla(0xA2);
      }
    }

    if ((valor > 25000) && (valor < 32000)) {  // Pos.3

      if (pos_ant_rot1 != 3) {

        pos_ant_rot1 = 3;
        mandar_tecla(0xA3);
      }
    }

    // rot 2
    valor = (uint16_t)(suma_rot2 / n_muestras);
    valor = map(valor, 0, 4095, 0, 65535);

#ifdef DEBUG_ANALOG
    chprintf(chp, "Valor rot2: %u \r\n", valor);
#endif

    if (valor < 7000) {  // Pos.0

      if (pos_ant_rot2 != 0) {

        pos_ant_rot2 = 0;
        mandar_tecla(0xB0);
      }
    }

    if ((valor > 7000) && (valor < 20000)) {  // Pos.1

      if (pos_ant_rot2 != 1) {

        pos_ant_rot2 = 1;
        mandar_tecla(0xB1);
      }
    }

// rot 3
    valor = (uint16_t)(suma_rot3 / n_muestras);
    valor = map(valor, 0, 4095, 0, 65535);

#ifdef DEBUG_ANALOG
    chprintf(chp, "Valor rot3: %u \r\n", valor);
#endif

    if (valor < 7000) {  // Pos.0

      if (pos_ant_rot3 != 0) {

        pos_ant_rot3 = 0;
        mandar_tecla(0xC0);
      }
    }

    if ((valor > 7000) && (valor < 18000)) {  // Pos.1

      if (pos_ant_rot3 != 1) {

        pos_ant_rot3 = 1;
        mandar_tecla(0xC1);
      }
    }

    if ((valor > 18000) && (valor < 25000)) {  // Pos.2

      if (pos_ant_rot3 != 2) {

        pos_ant_rot3 = 2;
        mandar_tecla(0xC2);
      }
    }

    if ((valor > 25000) && (valor < 30000)) {  // Pos.3

      if (pos_ant_rot3 != 3) {

        pos_ant_rot3 = 3;
        mandar_tecla(0xC3);
      }
    }

    if ((valor > 30000) && (valor < 35000)) {  // Pos.4

      if (pos_ant_rot3 != 4) {

        pos_ant_rot3 = 4;
        mandar_tecla(0xC4);
      }
    }

    if ((valor > 35000) && (valor < 40000)) {  // Pos.5

      if (pos_ant_rot3 != 5) {

        pos_ant_rot3 = 5;
        mandar_tecla(0xC5);
      }
    }

//rot 4
    valor = (uint16_t)(suma_rot4 / n_muestras);
    valor = map(valor, 0, 4095, 0, 65535);

#ifdef DEBUG_ANALOG
    chprintf(chp, "Valor rot4: %u \r\n", valor);
#endif

    if (valor < 7000) {  // Pos.0

      if (pos_ant_rot4 != 0) {

        pos_ant_rot4 = 0;
        mandar_tecla(0xD0);
      }
    }

    if ((valor > 7000) && (valor < 18000)) {  // Pos.1

      if (pos_ant_rot4 != 1) {

        pos_ant_rot4 = 1;
        mandar_tecla(0xD1);
      }
    }

    if ((valor > 18000) && (valor < 25000)) {  // Pos.2

      if (pos_ant_rot4 != 2) {

        pos_ant_rot4 = 2;
        mandar_tecla(0xD2);
      }
    }

    if ((valor > 25000) && (valor < 31000)) {  // Pos.3

      if (pos_ant_rot4 != 3) {

        pos_ant_rot4 = 3;
        mandar_tecla(0xD3);
      }
    }

    if ((valor > 31000) && (valor < 35000)) {  // Pos.4

      if (pos_ant_rot4 != 4) {

        pos_ant_rot4 = 4;
        mandar_tecla(0xD4);
      }
    }

    // Eje Y del mini-joystick

    valor = (uint16_t)(suma_eje_y / n_muestras);
    valor = map(valor, 0, 4095, 0, 65535);

#ifdef DEBUG_ANALOG
    chprintf(chp, "Valor eje Y: %u \r\n", valor);
#endif

    if (pos_ant_eje_x == 3) { // El otro eje tiene que estar en su centro.

      if (valor < 26000) {           // Pos.0

        if (pos_ant_eje_y != 0) {

          pos_ant_eje_y = 0;
          mandar_tecla(0xF0);
        }
      }

      if (valor > 55000) {          // Pos.1

        if (pos_ant_eje_y != 1) {

          pos_ant_eje_y = 1;
          mandar_tecla(0xF1);
        }

      }
    }

    if ((valor > 31000) && (valor < 35000)) {      // Pos. reposo

      pos_ant_eje_y = 3;

    }

    // Eje X del mini-joystick

    valor = (uint16_t)(suma_eje_x / n_muestras);
    valor = map(valor, 0, 4095, 0, 65535);

#ifdef DEBUG_ANALOG
    chprintf(chp, "Valor eje X: %u \r\n", valor);
#endif

    if (pos_ant_eje_y == 3) { // El otro eje tiene que estar en su centro.
      if (valor < 18000) {           // Pos.0

        if (pos_ant_eje_x != 0) {

          pos_ant_eje_x = 0;
          mandar_tecla(0xE0);
        }

      }

      if (valor > 40000) {          // Pos.1

        if (pos_ant_eje_x != 1) {

          pos_ant_eje_x = 1;
          mandar_tecla(0xE1);
        }

      }
    }

    if ((valor > 31000) && (valor < 35000)) {      // Pos. reposo

      pos_ant_eje_x = 3;

    }

    chThdSleepMilliseconds(20);
  }
}

/**
 * Hilo encargado de leer los 2 encoders.
 *
 */
static THD_FUNCTION(Leer_Encoders, arg) {
  chRegSetThreadName("Leer_Encoders");

  int8_t enc1_incremento = 0;
  int8_t enc2_incremento = 0;
  int8_t i = 0;

  tp = chThdGetSelfX();
  while (true) {
    chEvtWaitAny((eventmask_t )1);

    enc1_incremento = enc1_get_increment();
    enc2_incremento = enc2_get_increment();

    if (enc1_incremento != 0) {
      if (enc1_incremento > 0) {
        for (i = 0; i < enc1_incremento; i++) {
          mandar_tecla(0xE111);
        }

      }
      else {
        for (i = 0; i > enc1_incremento; i--) {
          mandar_tecla(0xE110);
        }

      }
    }

    if (enc2_incremento != 0) {
         if (enc2_incremento > 0) {
           for (i = 0; i < enc2_incremento; i++) {
             mandar_tecla(0xE211);
           }

         }
         else {
           for (i = 0; i > enc2_incremento; i--) {
             mandar_tecla(0xE210);
           }

         }
       }
  }
}


/**
 * Función encargada de mapear un valor de un rango a otro distinto.
 * Sólo acepta números de 16 bits sin signo.
 *
 * @param x Valor que se quiere mapear.
 * @param in_min Número mínimo del rango actual.
 * @param in_max Número máximo del rango actual.
 * @param out_min Número mínimo del rango deseado.
 * @param out_max Número máximo del rango deseado.
 * @return El nuevo valor mapeado al rango deseado.
 */
uint16_t map(uint16_t x, uint16_t in_min, uint16_t in_max, uint16_t out_min,
             uint16_t out_max) {
  return out_min + (x - in_min) * (out_max - out_min) / (in_max - in_min);
}

/**
 * Hilo encargado de esperar comunicación serie.
 */
static THD_FUNCTION(rx_serial, arg) {
  chRegSetThreadName("rx_serial");

  uint8_t trama_error[6];
  size_t tamano = 6;
  uint32_t comando;

  tp_rx = chThdGetSelfX();
  while (true) {
    eventmask_t evt = chEvtWaitAny(ALL_EVENTS);

    if (evt == 3)
      comando = leer_trama(rx_buffer_3);

    if (evt == 2)
      comando = leer_trama(rx_buffer_2);

    if (evt == 1)
      comando = leer_trama(rx_buffer_1);

    // TODO: Refactorizar mediante puntero al driver necesario en cada caso.

    if (comando == 0){    // Comando no válido.
      if (evt == 3){      // Desde placa número 2 (Comandos de Placa 2 y 3)
        prepara_trama(trama_error, 0xe7);
        tamano = 6;
        uartAcquireBus(&UARTD3);
        uartSendFullTimeout(&UARTD3, &tamano, &trama_error, MS2ST(25));// Se reenvía la trama de error.
        uartReleaseBus(&UARTD3);
      }

      if (evt == 2){     // Desde PC
        prepara_trama(trama_error, 0xe7);
        tamano = 6;
        uartAcquireBus(&UARTD2);
        uartSendFullTimeout(&UARTD2, &tamano, &trama_error, MS2ST(25));// Se reenvía la trama de error.
        uartReleaseBus(&UARTD2);
      }

      if (evt == 1){   // Desde placa número 1
        prepara_trama(trama_error, 0xe7);
        tamano = 6;
        uartAcquireBus(&UARTD1);
        uartSendFullTimeout(&UARTD1, &tamano, &trama_error, MS2ST(25));// Se reenvía la trama de error.
        uartReleaseBus(&UARTD1);
      }


    }else{

      if (comando == 0xe7){   // Se pide reenvío.

        if (evt == 3){
           prepara_trama(trama_error, 0xe7);
           tamano = 6;
           uartAcquireBus(&UARTD3);
           uartSendFullTimeout(&UARTD3, &tamano, &trama_error, MS2ST(25));// Se reenvía la trama de error.
           uartReleaseBus(&UARTD3);
         }

        if (evt == 2){
          tamano = 6;
          uartAcquireBus(&UARTD2);
          uartSendFullTimeout(&UARTD2, &tamano, &trama, MS2ST(25));// Se reenvía la trama.
          uartReleaseBus(&UARTD2);
          }

         if (evt == 1){
           prepara_trama(trama_error, 0xe7);
           tamano = 6;
           uartAcquireBus(&UARTD1);
           uartSendFullTimeout(&UARTD1, &tamano, &trama_error, MS2ST(25));// Se reenvía la trama de error.
           uartReleaseBus(&UARTD1);
         }

      }else{
        // El único comando válido aparte del de error es la tecla mandadda
        // por la placa 3 y la placa 2, por lo que se propaga hacia el PC.
        mandar_tecla(comando);

      }
    }

  }
}

/*
 * Se ejecuta cuando se el buffer de recepción está lleno.
 */
static void rxend1(UARTDriver *uartp) {

  (void)uartp;

  chSysLockFromISR();
  chEvtSignalI(tp_rx, (eventmask_t)1);
  uartStartReceiveI(&UARTD1, 6, &rx_buffer_1);
  chSysUnlockFromISR();
}

static void rxend2(UARTDriver *uartp) {

  (void)uartp;

  chSysLockFromISR();
  chEvtSignalI(tp_rx, (eventmask_t)2);
  uartStartReceiveI(&UARTD2, 6, &rx_buffer_2);
  chSysUnlockFromISR();
}

static void rxend3(UARTDriver *uartp) {

  (void)uartp;

  chSysLockFromISR();
  chEvtSignalI(tp_rx, (eventmask_t)3);
  uartStartReceiveI(&UARTD3, 6, &rx_buffer_3);
  chSysUnlockFromISR();
}
