/*
 * mcp23s17.c
 *
 *  Created on: 13/10/2016
 *      Author: JGReyes (www.circuiteando.net)
 *
 *  License: GPLv3 (www.gnu.org/licenses/gpl.html)
 *
 */
#include "mcp23s17.h"

/*
 * SPI configuration (256kHz, CPHA=0, CPOL=0, MSb first).
 */
static const SPIConfig spicfg = {
NULL,
GPIOB,
12,
SPI_CR1_BR_2 | SPI_CR1_BR_1 | SPI_CR1_BR_0};

/*
 * SPI TX and RX buffers.
 */
static uint8_t txbuf;
static uint8_t rxbuf;

void mcps_spi_init(void) {
  /*
   * SPI1 I/O pins setup.
   */

  palSetPadMode(GPIOB, 12, PAL_MODE_OUTPUT_PUSHPULL | PAL_STM32_OSPEED_HIGHEST); /* NSS.     */
  palSetPadMode(GPIOB, 13, PAL_MODE_ALTERNATE(5) | PAL_STM32_OSPEED_HIGHEST); /* SCK.     */
  palSetPadMode(GPIOB, 14, PAL_MODE_ALTERNATE(5)); /* MISO.    */
  palSetPadMode(GPIOB, 15, PAL_MODE_ALTERNATE(5) | PAL_STM32_OSPEED_HIGHEST); /* MOSI.    */
  palSetPad(GPIOB, 12);

  spiStart(&SPID2, &spicfg);

  init_exp012();
//  init_exp3();
//  init_exp4();
  init_exp5();
  init_exp6();

  // Se comprueba que se han mandado correctamente los datos
  // de lo contrario se vuelve a inicializar el expansor correspondiente.

  uint8_t pull_up_a = read(EXP_0, GPPUA);
  uint8_t pull_up_b = read(EXP_0, GPPUB);
  uint8_t pull_up_a_1 = read(EXP_1, GPPUA);
  uint8_t pull_up_b_1 = read(EXP_1, GPPUB);
  uint8_t pull_up_a_2 = read(EXP_2, GPPUA);
  uint8_t pull_up_b_2 = read(EXP_2, GPPUB);

  while ((pull_up_a != 0xff) || (pull_up_b != 0xff) || (pull_up_a_1 != 0xff) ||
      (pull_up_b_1 != 0xff) || (pull_up_a_2 != 0xff) || (pull_up_b_2 != 0xff)) {

    init_exp012();
    pull_up_a = read(EXP_0, GPPUA);
    pull_up_b = read(EXP_0, GPPUB);
    pull_up_a_1 = read(EXP_1, GPPUA);
    pull_up_b_1 = read(EXP_1, GPPUB);
    pull_up_a_2 = read(EXP_2, GPPUA);
    pull_up_b_2 = read(EXP_2, GPPUB);
  }

//  pull_up_a = read(EXP_3, GPPUA);
//  pull_up_b = read(EXP_3, GPPUB);
//
//  while ((pull_up_a != 0xff) || (pull_up_b != 0xff)) {
//    init_exp3();
//    pull_up_a = read(EXP_3, GPPUA);
//    pull_up_b = read(EXP_3, GPPUB);
//  }
//
//  pull_up_a = read(EXP_4, GPPUA);
//  pull_up_b = read(EXP_4, GPPUB);
//
//  while ((pull_up_a != 0xff) || (pull_up_b != 0xff)) {
//    init_exp4();
//    pull_up_a = read(EXP_4, GPPUA);
//    pull_up_b = read(EXP_4, GPPUB);
//  }

  pull_up_a = read(EXP_5, GPPUA);
  pull_up_b = read(EXP_5, GPPUB);

  while ((pull_up_a != 0xff) || (pull_up_b != 0xff)) {
    init_exp5();
    pull_up_a = read(EXP_5, GPPUA);
    pull_up_b = read(EXP_5, GPPUB);
  }

  pull_up_a = read(EXP_6, GPPUA);
  pull_up_b = read(EXP_6, GPPUB);

  while ((pull_up_a != 0xff) || (pull_up_b != 0xff)) {
    init_exp6();
    pull_up_a = read(EXP_6, GPPUA);
    pull_up_b = read(EXP_6, GPPUB);
  }

}

// Se inicializan en funciones separadas para poder reinicilizarlos de forma
// independiente si fuera necesario.

static void init_exp012(void) {
  palClearPad(GPIOA, GPIOA_rst_exp012);
  chThdSleepMilliseconds(10);
  palSetPad(GPIOA, GPIOA_rst_exp012);
  chThdSleepMilliseconds(1);
  write(EXP_0, IOCON, (IOCON_BANK |IOCON_DISSLW | IOCON_BYTE_MODE | IOCON_HAEN));
  write(EXP_0, GPPUA, 0xff);
  write(EXP_0, GPPUB, 0xff);

  write(EXP_1, IOCON, (IOCON_BANK | IOCON_DISSLW | IOCON_BYTE_MODE | IOCON_HAEN));
  write(EXP_1, GPPUA, 0xff);
  write(EXP_1, GPPUB, 0xff);

  write(EXP_2, IOCON, (IOCON_BANK | IOCON_DISSLW | IOCON_BYTE_MODE | IOCON_HAEN));
  write(EXP_2, GPPUA, 0xff);
  write(EXP_2, GPPUB, 0xff);
}

static void init_exp3(void) {
  palClearPad(GPIOC, GPIOC_rst_exp3);
  chThdSleepMilliseconds(10);
  palSetPad(GPIOC, GPIOC_rst_exp3);
  chThdSleepMilliseconds(1);
  write(EXP_3, IOCON, (IOCON_BANK | IOCON_DISSLW | IOCON_BYTE_MODE | IOCON_HAEN));
  write(EXP_3, IODIRA, 0xff);
  write(EXP_3, IODIRB, 0xff);
  write(EXP_3, GPPUA, 0xff);
  write(EXP_3, GPPUB, 0xff);
}

static void init_exp4(void) {
  palClearPad(GPIOC, GPIOC_rst_exp4); // Reset del expansor
  chThdSleepMilliseconds(10);
  palSetPad(GPIOC, GPIOC_rst_exp4);  // Activación del expansor
  chThdSleepMilliseconds(1);
  write(EXP_4, IOCON, (IOCON_BANK | IOCON_DISSLW | IOCON_BYTE_MODE | IOCON_HAEN)); // Direccionamiento por hardware y operaciones cambian entre los registros A y B.
  write(EXP_4, IODIRA, 0xff);
  write(EXP_4, IODIRB, 0xff);
  write(EXP_4, GPPUA, 0xff);  // Activa pullups del puerto A.
  write(EXP_4, GPPUB, 0xff);  // Activa pullups del puerto B.
}

static void init_exp5(void) {
  palClearPad(GPIOA, GPIOA_rst_exp5);
  chThdSleepMilliseconds(10);
  palSetPad(GPIOA, GPIOA_rst_exp5);
  chThdSleepMilliseconds(1);
  write(EXP_5, IOCON, (IOCON_BANK | IOCON_DISSLW | IOCON_BYTE_MODE | IOCON_HAEN));
  write(EXP_5, GPPUA, 0xff);
  write(EXP_5, GPPUB, 0xff);
}

static void init_exp6(void) {
  palClearPad(GPIOA, GPIOA_rst_exp6);
  chThdSleepMilliseconds(10);
  palSetPad(GPIOA, GPIOA_rst_exp6);
  chThdSleepMilliseconds(1);
  write(EXP_6, IOCON, (IOCON_BANK | IOCON_DISSLW | IOCON_BYTE_MODE | IOCON_HAEN));
  write(EXP_6, GPPUA, 0xff);
  write(EXP_6, GPPUB, 0xff);
}

/**
 * Función encargada de leer datos del expansor deseado.
 * @param exp_wr_opcode  Código formado por la dirección hardware y el bit de escritura del expansor deseado.
 * @param address        Dirección de memoria que se quiere leer.
 * @return               Devuelve el contenido de la dirección (8bits sin signo).
 */
uint8_t read(uint8_t exp_wr_opcode, uint8_t address) {

  uint8_t read_opcode = exp_wr_opcode | 1;  // LSB = 1 para leer.
  txbuf = read_opcode;
  spiSelect(&SPID2);
  spiSend(&SPID2, 1, &txbuf);
  txbuf = address;
  spiSend(&SPID2, 1, &txbuf);
  spiReceive(&SPID2, 1, &rxbuf);
  spiUnselect(&SPID2);
  return rxbuf;
}

/**
 * Función encargada de escribir datos en el expansor deseado.
 * @param exp_wr_opcode  Código formado por la dirección hardware y el bit de escritura del expansor deseado.
 * @param address        Dirección de memoria que se quiere escribir.
 * @param data           Dato que se quiere escribir (8bits sin signo).
 */
void write(uint8_t exp_wr_opcode, uint8_t address, uint8_t data) {

  txbuf = exp_wr_opcode;
  spiSelect(&SPID2);
  spiSend(&SPID2, 1, &txbuf);
  txbuf = address;
  spiSend(&SPID2, 1, &txbuf);
  txbuf = data;
  spiSend(&SPID2, 1, &txbuf);
  spiUnselect(&SPID2);

}

