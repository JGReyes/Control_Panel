/*
 * quadrature_encoder.c
 *
 *  Created on: 10/10/2016
 *      Author: JGReyes (www.circuiteando.net)
 *
 *  License: GPLv3 (www.gnu.org/licenses/gpl.html)
 */

#include "quadrature_encoder.h"
#include "main.h"

// PB6 TIM4 CH 1
// PB7 TIM4 CH 2
// PB8 TIM4 CH 3
// PB9 TIM4 CH 4

static uint8_t enc1_A_sig = 0;
static uint8_t enc1_B_sig = 1;
static uint8_t enc2_A_sig = 0;
static uint8_t enc2_B_sig = 1;

/**
 * Estructura para la configuración de las interrupciónes externas.
 */
static EXTConfig extcfg = {
   { {EXT_CH_MODE_DISABLED, NULL},
     {EXT_CH_MODE_DISABLED, NULL},
     {EXT_CH_MODE_DISABLED, NULL},
     {EXT_CH_MODE_DISABLED, NULL},
     {EXT_CH_MODE_DISABLED, NULL},
     {EXT_CH_MODE_DISABLED, NULL},
     {EXT_CH_MODE_RISING_EDGE | EXT_CH_MODE_AUTOSTART
        | EXT_MODE_GPIOB, a1_rise},
     {EXT_CH_MODE_RISING_EDGE | EXT_CH_MODE_AUTOSTART
        | EXT_MODE_GPIOB, b1_rise},
     {EXT_CH_MODE_RISING_EDGE | EXT_CH_MODE_AUTOSTART
        | EXT_MODE_GPIOB, a2_rise},
     {EXT_CH_MODE_RISING_EDGE | EXT_CH_MODE_AUTOSTART
        | EXT_MODE_GPIOB, b2_rise},
     {EXT_CH_MODE_DISABLED, NULL},
     {EXT_CH_MODE_DISABLED, NULL},
     {EXT_CH_MODE_DISABLED, NULL},
     {EXT_CH_MODE_DISABLED, NULL},
     {EXT_CH_MODE_DISABLED, NULL},
     {EXT_CH_MODE_DISABLED, NULL},
     {EXT_CH_MODE_DISABLED, NULL},
     {EXT_CH_MODE_DISABLED, NULL},
     {EXT_CH_MODE_DISABLED, NULL},
     {EXT_CH_MODE_DISABLED, NULL},
     {EXT_CH_MODE_DISABLED, NULL},
     {EXT_CH_MODE_DISABLED, NULL},
     {EXT_CH_MODE_DISABLED, NULL}
   }
};

/**
 * Estructura para cambiar la configuración de la interrupciones
 * asociadas al encoder 1.
 */
static EXTChannelConfig ch_config = {
EXT_CH_MODE_FALLING_EDGE | EXT_CH_MODE_AUTOSTART | EXT_MODE_GPIOB, a1_fall
};

/**
 * Estructura para cambiar la configuración de la interrupciones
 * asociadas al encoder 2.
 */
static EXTChannelConfig ch_configb = {
EXT_CH_MODE_FALLING_EDGE | EXT_CH_MODE_AUTOSTART | EXT_MODE_GPIOB, a2_fall

};

/**
 * Función asociada al flanco de subida del del canal A del encoder 1.
 */
void a1_rise(EXTDriver *extp, expchannel_t channel) {
  (void)extp;
  (void)channel;

  chSysLockFromISR();
  extChannelDisableI(&EXTD1, 6);
  enc1_A_sig = 1;

  if (enc1_B_sig == 0) {
    contador_enc1++;
  }
  else {
    contador_enc1--;
  }

  // Se cambia la configuración de las interrupciónes para que solo estén
  // habilitadas las que corresponde al siguiente estado válido del encoder.
  ch_config.mode = EXT_CH_MODE_FALLING_EDGE | EXT_CH_MODE_AUTOSTART
      | EXT_MODE_GPIOB;
  ch_config.cb = a1_fall;
  extSetChannelModeI(&EXTD1, 6, &ch_config);
  extChannelEnableI(&EXTD1, 6);
  chEvtSignalI(tp, (eventmask_t)1); // Se despierta el hilo que procesa los datos.
  chSysUnlockFromISR();
}

/**
 * Función asociada al flanco de bajada del del canal A del encoder 1.
 */
void a1_fall(EXTDriver *extp, expchannel_t channel) {
  (void)extp;
  (void)channel;

  chSysLockFromISR();
  extChannelDisableI(&EXTD1, 6);
  enc1_A_sig = 0;

  if (enc1_B_sig == 1) {
    contador_enc1++;
  }
  else {
    contador_enc1--;
  }
  ch_config.mode = EXT_CH_MODE_RISING_EDGE | EXT_CH_MODE_AUTOSTART
      | EXT_MODE_GPIOB;
  ch_config.cb = a1_rise;
  extSetChannelModeI(&EXTD1, 6, &ch_config);
  extChannelEnableI(&EXTD1, 6);
  chEvtSignalI(tp, (eventmask_t)1);
  chSysUnlockFromISR();
}

/**
 * Función asociada al flanco de subida del del canal B del encoder 1.
 */
void b1_rise(EXTDriver *extp, expchannel_t channel) {
  (void)extp;
  (void)channel;

  chSysLockFromISR();
  extChannelDisableI(&EXTD1, 7);
  enc1_B_sig = 1;

  if (enc1_A_sig == 1) {
    contador_enc1++;
  }
  else {
    contador_enc1--;
  }

  ch_config.mode = EXT_CH_MODE_FALLING_EDGE | EXT_CH_MODE_AUTOSTART
      | EXT_MODE_GPIOB;
  ch_config.cb = b1_fall;
  extSetChannelModeI(&EXTD1, 7, &ch_config);
  extChannelEnableI(&EXTD1, 7);
  chEvtSignalI(tp, (eventmask_t)1);
  chSysUnlockFromISR();
}

/**
 * Función asociada al flanco de bajada del del canal B del encoder 1.
 */
void b1_fall(EXTDriver *extp, expchannel_t channel) {
  (void)extp;
  (void)channel;

  chSysLockFromISR();
  extChannelDisableI(&EXTD1, 7);
  enc1_B_sig = 0;

  if (enc1_A_sig == 0) {
    contador_enc1++;
  }
  else {
    contador_enc1--;
  }
  ch_config.mode = EXT_CH_MODE_RISING_EDGE | EXT_CH_MODE_AUTOSTART
      | EXT_MODE_GPIOB;
  ch_config.cb = b1_rise;
  extSetChannelModeI(&EXTD1, 7, &ch_config);
  extChannelEnableI(&EXTD1, 7);
  chEvtSignalI(tp, (eventmask_t)1);
  chSysUnlockFromISR();
}

/**
 * Función asociada al flanco de subida del del canal A del encoder 2.
 */
void a2_rise(EXTDriver *extp, expchannel_t channel) {
  (void)extp;
  (void)channel;

  chSysLockFromISR();
  extChannelDisableI(&EXTD1, 8);
  enc2_A_sig = 1;

  if (enc2_B_sig == 0) {
    contador_enc2++;
  }
  else {
    contador_enc2--;
  }
  ch_configb.mode = EXT_CH_MODE_FALLING_EDGE | EXT_CH_MODE_AUTOSTART
      | EXT_MODE_GPIOB;
  ch_configb.cb = a2_fall;
  extSetChannelModeI(&EXTD1, 8, &ch_configb);
  extChannelEnableI(&EXTD1, 8);
  chEvtSignalI(tp, (eventmask_t)1);
  chSysUnlockFromISR();
}

/**
 * Función asociada al flanco de bajada del del canal A del encoder 2.
 */
void a2_fall(EXTDriver *extp, expchannel_t channel) {
  (void)extp;
  (void)channel;

  chSysLockFromISR();
  extChannelDisableI(&EXTD1, 8);
  enc2_A_sig = 0;

  if (enc2_B_sig == 1) {
    contador_enc2++;
  }
  else {
    contador_enc2--;
  }
  ch_configb.mode = EXT_CH_MODE_RISING_EDGE | EXT_CH_MODE_AUTOSTART
      | EXT_MODE_GPIOB;
  ch_configb.cb = a2_rise;
  extSetChannelModeI(&EXTD1, 8, &ch_configb);
  extChannelEnableI(&EXTD1, 8);
  chEvtSignalI(tp, (eventmask_t)1);
  chSysUnlockFromISR();
}

/**
 * Función asociada al flanco de subida del del canal B del encoder 2.
 */
void b2_rise(EXTDriver *extp, expchannel_t channel) {
  (void)extp;
  (void)channel;

  chSysLockFromISR();
  extChannelDisableI(&EXTD1, 9);
  enc2_B_sig = 1;

  if (enc2_A_sig == 1) {
    contador_enc2++;
  }
  else {
    contador_enc2--;
  }

  ch_configb.mode = EXT_CH_MODE_FALLING_EDGE | EXT_CH_MODE_AUTOSTART
      | EXT_MODE_GPIOB;
  ch_configb.cb = b2_fall;
  extSetChannelModeI(&EXTD1, 9, &ch_configb);
  extChannelEnableI(&EXTD1, 9);
  chEvtSignalI(tp, (eventmask_t)1);
  chSysUnlockFromISR();
}

/**
 * Función asociada al flanco de bajada del del canal B del encoder 2.
 */
void b2_fall(EXTDriver *extp, expchannel_t channel) {
  (void)extp;
  (void)channel;

  chSysLockFromISR();
  extChannelDisableI(&EXTD1, 9);
  enc2_B_sig = 0;

  if (enc2_A_sig == 0) {
    contador_enc2++;
  }
  else {
    contador_enc2--;
  }
  ch_configb.mode = EXT_CH_MODE_RISING_EDGE | EXT_CH_MODE_AUTOSTART
      | EXT_MODE_GPIOB;
  ch_configb.cb = b2_rise;
  extSetChannelModeI(&EXTD1, 9, &ch_configb);
  extChannelEnableI(&EXTD1, 9);
  chEvtSignalI(tp, (eventmask_t)1);
  chSysUnlockFromISR();
}

void initExtEnc(void) {
  extStart(&EXTD1, &extcfg);
}

/**
 * Función que devuelve el incremento del encoder 1.
 * @return Valor de 8 bits con signo.
 */
int8_t enc1_get_increment(void) {
  static int32_t prev_encoder_count = 0;
  int8_t increment = contador_enc1 - prev_encoder_count;
  prev_encoder_count = contador_enc1;
  return increment;
}

/**
 * Función que devuelve el incremento del encoder 2.
 * @return Valor de 8 bits con signo.
 */
int8_t enc2_get_increment(void) {
  static int32_t prev_encoder_count = 0;
  int8_t increment = contador_enc2 - prev_encoder_count;
  prev_encoder_count = contador_enc2;
  return increment;
}

