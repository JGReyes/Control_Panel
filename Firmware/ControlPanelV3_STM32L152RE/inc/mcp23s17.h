/*
 * mcp23s17.h
 *
 *  Created on: 13/10/2016
 *      Author: jose
 */

#ifndef MCP23S17_H_
#define MCP23S17_H_

#include "ch.h"
#include "hal.h"

#define INTERRUPT_POLARITY_BIT 0x02
#define INTERRUPT_MIRROR_BIT   0x40

// Registros configurados para BANK = 1 excepto IOCON para poder cambiar de banco.

#define IODIRA   0x00
#define IODIRB   0x10
#define GPINTENA 0x02
#define GPINTENB 0x12
#define DEFVALA  0x03
#define DEFVALB  0x13
#define INTCONA  0x04
#define INTCONB  0x14
#define IOCON    0x0A
#define GPPUA    0x06
#define GPPUB    0X16
#define INTFA    0x07
#define INTFB    0x17
#define INTCAPA  0x08
#define INTCAPB  0x18
#define GPIOA_   0x09
#define GPIOB_   0x19
#define OLATA    0x0A
#define OLATB    0x1A

// Control settings

#define IOCON_BANK  0x80 // Banked registers
#define IOCON_BYTE_MODE 0x20 // Disables sequential operation. If bank = 0, operations toggle between A and B registers
#define IOCON_HAEN  0x08 // Hardware address enable
#define IOCON_DISSLW 0X10 // Desabilita el slew rate de SDA.

// Direcciones de los expansores

#define EXP_0 0x40
#define EXP_1 0x42
#define EXP_2 0x44
#define EXP_3 0x46
#define EXP_4 0x48
#define EXP_5 0x4A
#define EXP_6 0x4C
#define PORT_A 0x00
#define PORT_B 0x01


extern uint8_t read(uint8_t exp_wr_opcode, uint8_t address);
extern void write(uint8_t exp_wr_opcode, uint8_t address, uint8_t data);

extern void mcps_spi_init(void);
static void init_exp012(void);
static void init_exp3(void);
static void init_exp4(void);
static void init_exp5(void);
static void init_exp6(void);

#endif /* MCP23S17_H_ */
