/*
 * quadrature_encoder.h
 *
 *  Created on: 10/10/2016
 *      Author: jose
 */

#ifndef QUADRATURE_ENCODER_H_
#define QUADRATURE_ENCODER_H_

#include "ch.h"
#include "hal.h"

int8_t enc1_get_increment(void);

int8_t enc2_get_increment(void);

void initExtEnc(void);

void a1_rise(EXTDriver *extp, expchannel_t channel);

void a1_fall(EXTDriver *extp, expchannel_t channel);

void b1_rise(EXTDriver *extp, expchannel_t channel);

void b1_fall(EXTDriver *extp, expchannel_t channel);

void a2_rise(EXTDriver *extp, expchannel_t channel);

void a2_fall(EXTDriver *extp, expchannel_t channel);

void b2_rise(EXTDriver *extp, expchannel_t channel);

void b2_fall(EXTDriver *extp, expchannel_t channel);



#endif /* QUADRATURE_ENCODER_H_ */
