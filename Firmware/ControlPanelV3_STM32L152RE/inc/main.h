/*
 * main.h
 *
 *  Created on: 7/10/2016
 *      Author: jose
 */

#ifndef MAIN_H_
#define MAIN_H_

//#define DEBUG_ANALOG



extern thread_t *tp;
extern int32_t contador_enc1;
extern int32_t contador_enc2;

static THD_FUNCTION(Leer_Nucleo_0, arg);

static THD_FUNCTION(Leer_Analog, arg);

static THD_FUNCTION(Leer_Encoders, arg);

static THD_FUNCTION(rx_serial, arg);

void mandar_tecla(uint32_t interruptor);

void configBit(uint8_t *variable, uint8_t posicion, bool valor);

uint16_t map(uint16_t x, uint16_t in_min, uint16_t in_max, uint16_t out_min,
             uint16_t out_max);

static void rxend1(UARTDriver *uartp);

static void rxend2(UARTDriver *uartp);

static void rxend3(UARTDriver *uartp);

void prepara_trama(uint8_t *trama_buffer, uint32_t comando);

uint32_t leer_trama(const uint8_t *trama_buffer);


#endif /* MAIN_H_ */
