/*
 * main.h
 *
 *  Created on: 7/10/2016
 *      Author: jose
 */

#ifndef MAIN_H_
#define MAIN_H_

//#define DEBUG_ANALOG


static THD_FUNCTION(Leer_Nucleo_2, arg);

static THD_FUNCTION(rx_serial, arg);

static void rxend1(UARTDriver *uartp);

static void rxend3(UARTDriver *uartp);

void mandar_tecla(uint32_t interruptor);

void configBit(uint8_t *variable, uint8_t posicion, bool valor);

void prepara_trama(uint8_t *trama_buffer, uint32_t comando);

uint32_t leer_trama(const uint8_t *trama_buffer);


#endif /* MAIN_H_ */
