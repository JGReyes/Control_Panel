/*
 * main.c
 *
 *  Created on: 21/10/2016
 *      Author: JGReyes (www.circuiteando.net)
 *
 *  License: GPLv3 (www.gnu.org/licenses/gpl.html)
 */

#include <stdio.h>
#include "ch.h"
#include "hal.h"
#include "main.h"
#include "button_debounce.h"
#include "lib_crc.h"

thread_t *tp = NULL;
uint8_t trama[6] = {0, 0, 0, 0, 0, 0};
uint8_t rx_buffer_1[6] = {0, 0, 0, 0, 0, 0};
uint8_t rx_buffer_3[6] = {0, 0, 0, 0, 0, 0};

/* Working areas for threads
 *
 */

static THD_WORKING_AREA(waLeer_Nucleo_2, 512);
static THD_WORKING_AREA(waSerial, 512);

WDGConfig wdgConf = {2,    // Prescaler. /16
    0xfff // Valor de recarga del contador. 1,77 segundos aprox.
    };

/*
 * UART driver configuration structure.
 */
static UARTConfig uart_cfg_1 = {
  NULL,
  NULL,
  rxend1,
  NULL,
  NULL,
  115200,
  0,
  USART_CR2_LINEN,
  0
};

/*
 * UART driver configuration structure.
 */
static UARTConfig uart_cfg_3 = {
  NULL,
  NULL,
  rxend3,
  NULL,
  NULL,
  115200,
  0,
  USART_CR2_LINEN,
  0
};

/* Main
 *
 */
int main(void) {


  halInit();
  chSysInit();

  wdgStart(&WDGD1, &wdgConf);

  uartStart(&UARTD1, &uart_cfg_1);
  uartStart(&UARTD3, &uart_cfg_3);

  chThdCreateStatic(waSerial, sizeof(waSerial), NORMALPRIO + 1, rx_serial,
                     NULL);

  chThdCreateStatic(waLeer_Nucleo_2, sizeof(waLeer_Nucleo_2), NORMALPRIO + 1,
                    Leer_Nucleo_2, NULL);


  chThdSleepMilliseconds(1000);
  uartStartReceive(&UARTD1, 6, &rx_buffer_1);
  uartStartReceive(&UARTD3, 6, &rx_buffer_3);

  while (true) {
    chThdSleepMilliseconds(5000);
  }
}

/**
 * Cambia un bit especifico en una variable de 8 bits.
 *
 * @param variable  Variable que se quiere modificar.
 * @param posicion  Posición del bit 0..7.
 * @param valor     Valor que tendrá el bit true (1) o false (0)
 */
void configBit(uint8_t *variable, uint8_t posicion, bool valor) {

  if (valor) {
    *variable |= (1 << posicion);

  }
  else {
    *variable &= ~(1 << posicion);
  }

}

/**
 * Manda por puerto serie el comando asociado a la tecla pulsada.
 *
 * @param interruptor Número uint32 de la tecla pulsada.
 */
void mandar_tecla(uint32_t interruptor) {

  size_t tamano = 6;
  prepara_trama(trama, interruptor);
  uartAcquireBus(&UARTD1);
  uartSendFullTimeout(&UARTD1, &tamano, &trama, MS2ST(25));
  uartReleaseBus(&UARTD1);
}

/**
 * Función encargada de prepara una trama para enviar un comando.
 * @param trama_buffer  Buffer que contendrá la trama a enviar.
 * @param comando       Comando que se quiere enviar.
 */
void prepara_trama(uint8_t *trama_buffer, uint32_t comando){
    uint8_t cmd[3] = {0, 0, 0};
    cmd[0] = comando;
    cmd[1] = (comando >> 8);
    cmd[2] = (comando >> 16);

    uint16_t crc = calculate_crc16(cmd, 3);

    trama_buffer[0] = 0x7e;            // Byte de sincronización
    trama_buffer[1] = cmd[2];          // Interruptor MSB primero 3 Bytes
    trama_buffer[2] = cmd[1];
    trama_buffer[3] = cmd[0];
    trama_buffer[4] = (crc >> 8);     // Crc MSB
    trama_buffer[5] = crc;            // Crc LSB

}

/**
 * Función encargada de recuperar el comando dentro de la trama.
 * @param trama_buffer  Buffer que contiene la trama a leer.
 * @return              Comando de la trama, 0 en caso de error.
 */
uint32_t leer_trama(const uint8_t *trama_buffer){
  if (trama_buffer[0] == 0x7e) {

       uint32_t comando = (trama_buffer[1] << 16) | (trama_buffer[2] << 8)
           | (trama_buffer[3]);

       uint16_t crc = (trama_buffer[4] << 8) | (trama_buffer[5]);

       uint8_t cmd[3] = {0, 0, 0};
       cmd[0] = comando;
       cmd[1] = (comando >> 8);
       cmd[2] = (comando >> 16);

       if (calculate_crc16(cmd, 3) == crc) {
         /* Trama válida */
         return comando;
       }else{
         /*Crc erróneo */
         return 0;
       }
  }else{
    /* Sin byte de sincronización, trama nó válida*/
    return 0;
  }
}

/**
 * Es un hilo de ejecución que lee las entradas y manda los comandos correspondientes.
 *
 */
static THD_FUNCTION(Leer_Nucleo_2, arg) {

  chRegSetThreadName("Leer_Nucleo_2");

  uint8_t bloque1 = 0;
  uint8_t bloque2 = 0;
  uint8_t bloque3 = 0;
  uint8_t bloque4 = 0;
  uint8_t bloque5 = 0;
  uint8_t bloque6 = 0;
  uint8_t bloque7 = 0;

  Debouncer dbr_bloque1;
  Debouncer dbr_bloque2;
  Debouncer dbr_bloque3;
  Debouncer dbr_bloque4;
  Debouncer dbr_bloque5;
  Debouncer dbr_bloque6;
  Debouncer dbr_bloque7;

  ButtonDebounceInit(&dbr_bloque1, 0xff);
  ButtonDebounceInit(&dbr_bloque2, 0xff);
  ButtonDebounceInit(&dbr_bloque3, 0xff);
  ButtonDebounceInit(&dbr_bloque4, 0xff);
  ButtonDebounceInit(&dbr_bloque5, 0xff);
  ButtonDebounceInit(&dbr_bloque6, 0xff);
  ButtonDebounceInit(&dbr_bloque7, 0xff);

  while (true) {

    uint16_t porta = palReadGroup(GPIOA, 0x99f3, 0);

    /* Bloque 1 */
    bloque1 = porta;

    /* Bloque 2 */
    bloque2 = (porta >> 8);

    uint16_t portb = palReadGroup(GPIOB, 0xf3f7, 0);
    /* Bloque 3 */
    bloque3 = portb;

    /* Bloque 4 */
    bloque4 = (portb >> 8);

    uint16_t portc = palReadGroup(GPIOC, 0x3fff, 0);
    /* Bloque 5 */
    bloque5 = portc;

    /* Bloque 6 */
    bloque6 = (portc >> 8);

    uint16_t portd = palReadGroup(GPIOD, 0x0004, 0);
    /* Bloque 7 */
    bloque7 = portd;

    ButtonProcess(&dbr_bloque1, bloque1);
    ButtonProcess(&dbr_bloque2, bloque2);
    ButtonProcess(&dbr_bloque3, bloque3);
    ButtonProcess(&dbr_bloque4, bloque4);
    ButtonProcess(&dbr_bloque5, bloque5);
    ButtonProcess(&dbr_bloque6, bloque6);
    ButtonProcess(&dbr_bloque7, bloque7);

    /* Bloque 1 Pressed */

    if (ButtonPressed(&dbr_bloque1, BUTTON_PIN_0)) {
      mandar_tecla(0x2A100);

    }

    if (ButtonPressed(&dbr_bloque1, BUTTON_PIN_1)) {
      mandar_tecla(0x2A110);

    }

    if (ButtonPressed(&dbr_bloque1, BUTTON_PIN_2)) {
      mandar_tecla(0x2A120);

    }

    if (ButtonPressed(&dbr_bloque1, BUTTON_PIN_3)) {
      mandar_tecla(0x2A130);

    }

    if (ButtonPressed(&dbr_bloque1, BUTTON_PIN_4)) {
      mandar_tecla(0x2A140);

    }

    if (ButtonPressed(&dbr_bloque1, BUTTON_PIN_5)) {
      mandar_tecla(0x2A150);

    }

    if (ButtonPressed(&dbr_bloque1, BUTTON_PIN_6)) {
      mandar_tecla(0x2A160);

    }

    if (ButtonPressed(&dbr_bloque1, BUTTON_PIN_7)) {
      mandar_tecla(0x2A170);

    }

    /* Bloque 1 Released */

    if (ButtonReleased(&dbr_bloque1, BUTTON_PIN_0)) {
      mandar_tecla(0x2A101);

    }

    if (ButtonReleased(&dbr_bloque1, BUTTON_PIN_1)) {
      mandar_tecla(0x2A111);

    }

    if (ButtonReleased(&dbr_bloque1, BUTTON_PIN_2)) {
      mandar_tecla(0x2A121);

    }

    if (ButtonReleased(&dbr_bloque1, BUTTON_PIN_3)) {
      mandar_tecla(0x2A131);

    }

    if (ButtonReleased(&dbr_bloque1, BUTTON_PIN_4)) {
      mandar_tecla(0x2A141);

    }

    if (ButtonReleased(&dbr_bloque1, BUTTON_PIN_5)) {
      mandar_tecla(0x2A151);

    }

    if (ButtonReleased(&dbr_bloque1, BUTTON_PIN_6)) {
      mandar_tecla(0x2A161);

    }

    if (ButtonReleased(&dbr_bloque1, BUTTON_PIN_7)) {
      mandar_tecla(0x2A171);

    }

    /* Bloque 2 Pressed */

    if (ButtonPressed(&dbr_bloque2, BUTTON_PIN_0)) {
      mandar_tecla(0x2A200);

    }

    if (ButtonPressed(&dbr_bloque2, BUTTON_PIN_1)) {
      mandar_tecla(0x2A210);

    }

    if (ButtonPressed(&dbr_bloque2, BUTTON_PIN_2)) {
      mandar_tecla(0x2A220);

    }

    if (ButtonPressed(&dbr_bloque2, BUTTON_PIN_3)) {
      mandar_tecla(0x2A230);

    }

    if (ButtonPressed(&dbr_bloque2, BUTTON_PIN_4)) {
      mandar_tecla(0x2A240);

    }

    if (ButtonPressed(&dbr_bloque2, BUTTON_PIN_5)) {
      mandar_tecla(0x2A250);

    }

    if (ButtonPressed(&dbr_bloque2, BUTTON_PIN_6)) {
      mandar_tecla(0x2A260);

    }

    if (ButtonPressed(&dbr_bloque2, BUTTON_PIN_7)) {
      mandar_tecla(0x2A270);

    }

    /* Bloque 2 Released */

    if (ButtonReleased(&dbr_bloque2, BUTTON_PIN_0)) {
      mandar_tecla(0x2A201);

    }

    if (ButtonReleased(&dbr_bloque2, BUTTON_PIN_1)) {
      mandar_tecla(0x2A211);

    }

    if (ButtonReleased(&dbr_bloque2, BUTTON_PIN_2)) {
      mandar_tecla(0x2A221);

    }

    if (ButtonReleased(&dbr_bloque2, BUTTON_PIN_3)) {
      mandar_tecla(0x2A231);

    }

    if (ButtonReleased(&dbr_bloque2, BUTTON_PIN_4)) {
      mandar_tecla(0x2A241);

    }

    if (ButtonReleased(&dbr_bloque2, BUTTON_PIN_5)) {
      mandar_tecla(0x2A251);

    }

    if (ButtonReleased(&dbr_bloque2, BUTTON_PIN_6)) {
      mandar_tecla(0x2A261);

    }

    if (ButtonReleased(&dbr_bloque2, BUTTON_PIN_7)) {
      mandar_tecla(0x2A271);

    }

    /* Bloque 3 Pressed */

    if (ButtonPressed(&dbr_bloque3, BUTTON_PIN_0)) {
      mandar_tecla(0x2B300);

    }

    if (ButtonPressed(&dbr_bloque3, BUTTON_PIN_1)) {
      mandar_tecla(0x2B310);

    }

    if (ButtonPressed(&dbr_bloque3, BUTTON_PIN_2)) {
      mandar_tecla(0x2B320);

    }

    if (ButtonPressed(&dbr_bloque3, BUTTON_PIN_3)) {
      mandar_tecla(0x2B330);

    }

    if (ButtonPressed(&dbr_bloque3, BUTTON_PIN_4)) {
      mandar_tecla(0x2B340);

    }

    if (ButtonPressed(&dbr_bloque3, BUTTON_PIN_5)) {
      mandar_tecla(0x2B350);

    }

    if (ButtonPressed(&dbr_bloque3, BUTTON_PIN_6)) {
      mandar_tecla(0x2B360);

    }

    if (ButtonPressed(&dbr_bloque3, BUTTON_PIN_7)) {
      mandar_tecla(0x2B370);

    }

    /* Bloque 3 Released */

    if (ButtonReleased(&dbr_bloque3, BUTTON_PIN_0)) {
      mandar_tecla(0x2B301);

    }

    if (ButtonReleased(&dbr_bloque3, BUTTON_PIN_1)) {
      mandar_tecla(0x2B311);

    }

    if (ButtonReleased(&dbr_bloque3, BUTTON_PIN_2)) {
      mandar_tecla(0x2B321);

    }

    if (ButtonReleased(&dbr_bloque3, BUTTON_PIN_3)) {
      mandar_tecla(0x2B331);

    }

    if (ButtonReleased(&dbr_bloque3, BUTTON_PIN_4)) {
      mandar_tecla(0x2B341);

    }

    if (ButtonReleased(&dbr_bloque3, BUTTON_PIN_5)) {
      mandar_tecla(0x2B351);

    }

    if (ButtonReleased(&dbr_bloque3, BUTTON_PIN_6)) {
      mandar_tecla(0x2B361);

    }

    if (ButtonReleased(&dbr_bloque3, BUTTON_PIN_7)) {
      mandar_tecla(0x2B371);

    }

    /* Bloque 4 Pressed */

    if (ButtonPressed(&dbr_bloque4, BUTTON_PIN_0)) {
      mandar_tecla(0x2B400);

    }

    if (ButtonPressed(&dbr_bloque4, BUTTON_PIN_1)) {
      mandar_tecla(0x2B410);

    }

    if (ButtonPressed(&dbr_bloque4, BUTTON_PIN_2)) {
      mandar_tecla(0x2B420);

    }

    if (ButtonPressed(&dbr_bloque4, BUTTON_PIN_3)) {
      mandar_tecla(0x2B430);

    }

    if (ButtonPressed(&dbr_bloque4, BUTTON_PIN_4)) {
      mandar_tecla(0x2B440);

    }

    if (ButtonPressed(&dbr_bloque4, BUTTON_PIN_5)) {
      mandar_tecla(0x2B450);

    }

    if (ButtonPressed(&dbr_bloque4, BUTTON_PIN_6)) {
      mandar_tecla(0x2B460);

    }

    if (ButtonPressed(&dbr_bloque4, BUTTON_PIN_7)) {
      mandar_tecla(0x2B470);

    }

    /* Bloque 4 Released */

    if (ButtonReleased(&dbr_bloque4, BUTTON_PIN_0)) {
      mandar_tecla(0x2B401);

    }

    if (ButtonReleased(&dbr_bloque4, BUTTON_PIN_1)) {
      mandar_tecla(0x2B411);

    }

    if (ButtonReleased(&dbr_bloque4, BUTTON_PIN_2)) {
      mandar_tecla(0x2B421);

    }

    if (ButtonReleased(&dbr_bloque4, BUTTON_PIN_3)) {
      mandar_tecla(0x2B431);

    }

    if (ButtonReleased(&dbr_bloque4, BUTTON_PIN_4)) {
      mandar_tecla(0x2B441);

    }

    if (ButtonReleased(&dbr_bloque4, BUTTON_PIN_5)) {
      mandar_tecla(0x2B451);

    }

    if (ButtonReleased(&dbr_bloque4, BUTTON_PIN_6)) {
      mandar_tecla(0x2B461);

    }

    if (ButtonReleased(&dbr_bloque4, BUTTON_PIN_7)) {
      mandar_tecla(0x2B471);

    }

    /* Bloque 5 Pressed */

    if (ButtonPressed(&dbr_bloque5, BUTTON_PIN_0)) {
      mandar_tecla(0x2C500);

    }

    if (ButtonPressed(&dbr_bloque5, BUTTON_PIN_1)) {
      mandar_tecla(0x2C510);

    }

    if (ButtonPressed(&dbr_bloque5, BUTTON_PIN_2)) {
      mandar_tecla(0x2C520);

    }

    if (ButtonPressed(&dbr_bloque5, BUTTON_PIN_3)) {
      mandar_tecla(0x2C530);

    }

    if (ButtonPressed(&dbr_bloque5, BUTTON_PIN_4)) {
      mandar_tecla(0x2C540);

    }

    if (ButtonPressed(&dbr_bloque5, BUTTON_PIN_5)) {
      mandar_tecla(0x2C550);

    }

    if (ButtonPressed(&dbr_bloque5, BUTTON_PIN_6)) {
      mandar_tecla(0x2C560);

    }

    if (ButtonPressed(&dbr_bloque5, BUTTON_PIN_7)) {
      mandar_tecla(0x2C570);

    }

    /* Bloque 5 Released */

    if (ButtonReleased(&dbr_bloque5, BUTTON_PIN_0)) {
      mandar_tecla(0x2C501);

    }

    if (ButtonReleased(&dbr_bloque5, BUTTON_PIN_1)) {
      mandar_tecla(0x2C511);

    }

    if (ButtonReleased(&dbr_bloque5, BUTTON_PIN_2)) {
      mandar_tecla(0x2C521);

    }

    if (ButtonReleased(&dbr_bloque5, BUTTON_PIN_3)) {
      mandar_tecla(0x2C531);

    }

    if (ButtonReleased(&dbr_bloque5, BUTTON_PIN_4)) {
      mandar_tecla(0x2C541);

    }

    if (ButtonReleased(&dbr_bloque5, BUTTON_PIN_5)) {
      mandar_tecla(0x2C551);

    }

    if (ButtonReleased(&dbr_bloque5, BUTTON_PIN_6)) {
      mandar_tecla(0x2C561);

    }

    if (ButtonReleased(&dbr_bloque5, BUTTON_PIN_7)) {
      mandar_tecla(0x2C571);

    }

    /* Bloque 6 Pressed */

    if (ButtonPressed(&dbr_bloque6, BUTTON_PIN_0)) {
      mandar_tecla(0x2C600);

    }

    if (ButtonPressed(&dbr_bloque6, BUTTON_PIN_1)) {
      mandar_tecla(0x2C610);

    }

    if (ButtonPressed(&dbr_bloque6, BUTTON_PIN_2)) {
      mandar_tecla(0x2C620);

    }

    if (ButtonPressed(&dbr_bloque6, BUTTON_PIN_3)) {
      mandar_tecla(0x2C630);

    }

    if (ButtonPressed(&dbr_bloque6, BUTTON_PIN_4)) {
      mandar_tecla(0x2C640);

    }

    if (ButtonPressed(&dbr_bloque6, BUTTON_PIN_5)) {
      mandar_tecla(0x2C650);

    }

    if (ButtonPressed(&dbr_bloque6, BUTTON_PIN_6)) {
      mandar_tecla(0x2C660);

    }

    if (ButtonPressed(&dbr_bloque6, BUTTON_PIN_7)) {
      mandar_tecla(0x2C670);

    }

    /* Bloque 6 Released */

    if (ButtonReleased(&dbr_bloque6, BUTTON_PIN_0)) {
      mandar_tecla(0x2C601);

    }

    if (ButtonReleased(&dbr_bloque6, BUTTON_PIN_1)) {
      mandar_tecla(0x2C611);

    }

    if (ButtonReleased(&dbr_bloque6, BUTTON_PIN_2)) {
      mandar_tecla(0x2C621);

    }

    if (ButtonReleased(&dbr_bloque6, BUTTON_PIN_3)) {
      mandar_tecla(0x2C631);

    }

    if (ButtonReleased(&dbr_bloque6, BUTTON_PIN_4)) {
      mandar_tecla(0x2C641);

    }

    if (ButtonReleased(&dbr_bloque6, BUTTON_PIN_5)) {
      mandar_tecla(0x2C651);

    }

    if (ButtonReleased(&dbr_bloque6, BUTTON_PIN_6)) {
      mandar_tecla(0x2C661);

    }

    if (ButtonReleased(&dbr_bloque6, BUTTON_PIN_7)) {
      mandar_tecla(0x2C671);

    }

    /* Bloque 7 Pressed */

    if (ButtonPressed(&dbr_bloque7, BUTTON_PIN_2)) {
      mandar_tecla(0x2D720);

    }

    /* Bloque 7 Released */

    if (ButtonReleased(&dbr_bloque7, BUTTON_PIN_2)) {
      mandar_tecla(0x2D721);

    }

    wdgReset(&WDGD1); // Reset del watchdog.
    chThdSleepMilliseconds(5);

  }
}

/**
 * Hilo encargado de esperar comunicación serie para mandar de nuevo la
 * última trama de datos en caso de error.
 */
static THD_FUNCTION(rx_serial, arg) {
  chRegSetThreadName("rx_serial");

  uint8_t trama_error[6];
  size_t tamano = 6;
  uint32_t comando;

  tp = chThdGetSelfX();
  while (true) {
    eventmask_t evt = chEvtWaitAny(ALL_EVENTS);

    if (evt == 3)
      comando = leer_trama(rx_buffer_3);

    if (evt == 1)
      comando = leer_trama(rx_buffer_1);

    if (comando == 0){
      if (evt == 3){
        prepara_trama(trama_error, 0xe7);
        tamano = 6;
        uartAcquireBus(&UARTD3);
        uartSendFullTimeout(&UARTD3, &tamano, &trama_error, MS2ST(25));// Se reenvía la trama de error.
        uartReleaseBus(&UARTD3);
      }

      if (evt == 1){
        prepara_trama(trama_error, 0xe7);
        tamano = 6;
        uartAcquireBus(&UARTD1);
        uartSendFullTimeout(&UARTD1, &tamano, &trama_error, MS2ST(25));// Se reenvía la trama de error.
        uartReleaseBus(&UARTD1);
      }


    }else{

      if (comando == 0xe7){

        if (evt == 3){
           prepara_trama(trama_error, 0xe7);
           tamano = 6;
           uartAcquireBus(&UARTD3);
           uartSendFullTimeout(&UARTD3, &tamano, &trama_error, MS2ST(25));// Se reenvía la trama de error.
           uartReleaseBus(&UARTD3);
         }

         if (evt == 1){
           tamano = 6;
           uartAcquireBus(&UARTD1);
           uartSendFullTimeout(&UARTD1, &tamano, &trama, MS2ST(25));// Se reenvía la trama.
           uartReleaseBus(&UARTD1);
         }

      }else{
        // El único comando válido aparte del de error es la tecla mandadda
        // por la placa 3, por lo que se propaga hacia la 1.
        mandar_tecla(comando);

      }
    }

  }
}

/*
 * Se ejecuta cuando se el buffer de recepción está lleno.
 */
static void rxend1(UARTDriver *uartp) {

  (void)uartp;

  chSysLockFromISR();
  chEvtSignalI(tp, (eventmask_t)1);
  uartStartReceiveI(&UARTD1, 6, &rx_buffer_1);
  chSysUnlockFromISR();
}

static void rxend3(UARTDriver *uartp) {

  (void)uartp;

  chSysLockFromISR();
  chEvtSignalI(tp, (eventmask_t)3);
  uartStartReceiveI(&UARTD3, 6, &rx_buffer_3);
  chSysUnlockFromISR();
}
