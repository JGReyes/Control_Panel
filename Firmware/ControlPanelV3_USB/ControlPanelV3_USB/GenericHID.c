/*
 * GenericHID.c
 *
 *  Created on: 21/11/2016
 *      Author: JGReyes (www.circuiteando.net)
 *
 *  License: GPLv3 (www.gnu.org/licenses/gpl.html)
 */

#include "GenericHID.h"
#include "src/LUFA/LUFA/Drivers/Peripheral/Serial.h"

/** Buffer to hold the previously generated HID report, for comparison purposes inside the HID class driver. */
static uint8_t PrevHIDReportBuffer[GENERIC_REPORT_SIZE];

/** LUFA HID Class driver interface configuration and state information. This structure is
 *  passed to all HID Class driver functions, so that multiple instances of the same class
 *  within a device can be differentiated from one another.
 */
USB_ClassInfo_HID_Device_t Generic_HID_Interface =
	{
		.Config =
			{
				.InterfaceNumber              = INTERFACE_ID_GenericHID,
				.ReportINEndpoint             =
					{
						.Address              = GENERIC_IN_EPADDR,
						.Size                 = GENERIC_EPSIZE,
						.Banks                = 1,
					},
				.PrevReportINBuffer           = PrevHIDReportBuffer,
				.PrevReportINBufferSize       = sizeof(PrevHIDReportBuffer),
			},
	};

static uint8_t trama_rx[6]; // Buffer de recepci�n serie.
static uint8_t trama_tx[6]; // Buffer para la transmisic�n serie.
bool bufferListo = false;   // Indica si el buffer usb contiene datos para el PC.
bool transmitir = false;    // Indica si se tienen que transmitir datos al panel por el puerto serie.

/** Main program entry point. This routine contains the overall program flow, including initial
 *  setup of all components and the main program loop.
 */
int main(void)
{
	SetupHardware();
  
  // Declaraci�n de variables
 
  bool recibiendo_trama = false; // Indica si se est� recibiendo una trama de datos desde el panel.
  int16_t byte_recibido = 0;    
  uint8_t n_bytes = 0;           // N�mero de bytes recibidos desde el panel.
  
	LEDs_SetAllLEDs(LEDMASK_USB_NOTREADY);
	GlobalInterruptEnable();
  
  // Configuraci�n del puerto serie a 57.600 bps
  Serial_Init(57600, false);

	for (;;)
	{
		HID_Device_USBTask(&Generic_HID_Interface);
		USB_USBTask();
    
    byte_recibido = Serial_ReceiveByte();
    if (byte_recibido > -1)                   // Si se ha recibido un byte
    {
      if (byte_recibido == 0x7e)              // E indica comienzo de trama.
      {
        recibiendo_trama = true;
        n_bytes = 1;
        trama_rx[n_bytes-1] = (uint8_t) byte_recibido;   // Guarda el byte en el buffer.
      }
      else
      {
        if (recibiendo_trama)                // Si el byte recibido es distinto del de comienzo, pero se est� recibiendo una trama.
        {
          n_bytes++;
          trama_rx[n_bytes-1] = (uint8_t) byte_recibido; // Guarda el byte en el buffer.
        }
        else                       // Si el byte es distinto del de comienzo y no se est� recibiendo una trama. Es un error de transmisi�n.
        {
         envio_panel(true);       // Se pide el reenv�o de la �ltima trama.
        }
      }
    }
    
    if (n_bytes == 6)  // Si se han recibido 6 bytes, la trama est� completa.
    {
      bufferListo = true;
      recibiendo_trama = false;
      n_bytes = 0;
    }
    
    if (transmitir)  // Si han llegado datos del PC por USB, se transmiten al panel por puerto serie.
    {
      transmitir = false;
      envio_panel(false);
    }
	}
}

// �sta funci�n env�a los datos recibidos por el puerto USB al panel por el puerto serie,
// si envioError est� en false (por defecto).
// Si se pone a true, se carga el buffer con una trama de petici�n de reenvi� de datos(error),
// para que el panel env�e el �ltimo comando transmitido.
void envio_panel(bool envioError)
{
  if (envioError )
  {
    trama_tx[0] = 0x7e;
    trama_tx[1] = 0x00;
    trama_tx[2] = 0x00;
    trama_tx[3] = 0xe7;
    trama_tx[4] = 0x37;
    trama_tx[5] = 0xb0;
  }
  
  Serial_SendData(trama_tx, 6);
}

/** Configures the board hardware and chip peripherals for the demo's functionality. */
void SetupHardware(void)
{
#if (ARCH == ARCH_AVR8)
	/* Disable watchdog if enabled by bootloader/fuses */
	MCUSR &= ~(1 << WDRF);
	wdt_disable();

	/* Disable clock division */
	clock_prescale_set(clock_div_1);
#endif

	/* Hardware Initialization */
	LEDs_Init();
	USB_Init();
}

/** Event handler for the library USB Connection event. */
void EVENT_USB_Device_Connect(void)
{
	LEDs_SetAllLEDs(LEDMASK_USB_ENUMERATING);
}

/** Event handler for the library USB Disconnection event. */
void EVENT_USB_Device_Disconnect(void)
{
	LEDs_SetAllLEDs(LEDMASK_USB_NOTREADY);
}

/** Event handler for the library USB Configuration Changed event. */
void EVENT_USB_Device_ConfigurationChanged(void)
{
	bool ConfigSuccess = true;

	ConfigSuccess &= HID_Device_ConfigureEndpoints(&Generic_HID_Interface);

	USB_Device_EnableSOFEvents();

	LEDs_SetAllLEDs(ConfigSuccess ? LEDMASK_USB_READY : LEDMASK_USB_ERROR);
}

/** Event handler for the library USB Control Request reception event. */
void EVENT_USB_Device_ControlRequest(void)
{
	HID_Device_ProcessControlRequest(&Generic_HID_Interface);
}

/** Event handler for the USB device Start Of Frame event. */
void EVENT_USB_Device_StartOfFrame(void)
{
	HID_Device_MillisecondElapsed(&Generic_HID_Interface);
}

/** HID class driver callback function for the creation of HID reports to the host.
 *
 *  \param[in]     HIDInterfaceInfo  Pointer to the HID class interface configuration structure being referenced
 *  \param[in,out] ReportID    Report ID requested by the host if non-zero, otherwise callback should set to the generated report ID
 *  \param[in]     ReportType  Type of the report to create, either HID_REPORT_ITEM_In or HID_REPORT_ITEM_Feature
 *  \param[out]    ReportData  Pointer to a buffer where the created report should be stored
 *  \param[out]    ReportSize  Number of bytes written in the report (or zero if no report is to be sent)
 *
 *  \return Boolean \c true to force the sending of the report, \c false to let the library determine if it needs to be sent
 */
bool CALLBACK_HID_Device_CreateHIDReport(USB_ClassInfo_HID_Device_t* const HIDInterfaceInfo,
                                         uint8_t* const ReportID,
                                         const uint8_t ReportType,
                                         void* ReportData,
                                         uint16_t* const ReportSize)
{
  
	uint8_t* Data        = (uint8_t*)ReportData;
	
  if (bufferListo)            // Si el buffer rx tiene una trama, se carga en el buffer USB. 
  {
    Data[0] = trama_rx[0];
    Data[1] = trama_rx[1];
    Data[2] = trama_rx[2];
    Data[3] = trama_rx[3];
    Data[4] = trama_rx[4];
    Data[5] = trama_rx[5];
  }
  else                      // De no contener un trama completa, se pone a 0.
  {
    Data[0] = 0;
    Data[1] = 0;
    Data[2] = 0;
    Data[3] = 0;
    Data[4] = 0;
    Data[5] = 0;
  }

	*ReportSize = GENERIC_REPORT_SIZE;
	return false;
}

/** HID class driver callback function for the processing of HID reports from the host.
 *
 *  \param[in] HIDInterfaceInfo  Pointer to the HID class interface configuration structure being referenced
 *  \param[in] ReportID    Report ID of the received report from the host
 *  \param[in] ReportType  The type of report that the host has sent, either HID_REPORT_ITEM_Out or HID_REPORT_ITEM_Feature
 *  \param[in] ReportData  Pointer to a buffer where the received report has been stored
 *  \param[in] ReportSize  Size in bytes of the received HID report
 */
void CALLBACK_HID_Device_ProcessHIDReport(USB_ClassInfo_HID_Device_t* const HIDInterfaceInfo,
                                          const uint8_t ReportID,
                                          const uint8_t ReportType,
                                          const void* ReportData,
                                          const uint16_t ReportSize)
{
  
	uint8_t* Data       = (uint8_t*)ReportData;
	
  if (Data[0] == 0x7e)          // Si se recibe del PC un byte con 0x7e, es que se est� recibiendo una trama.
  {                             // Por lo que carga en el buffer tx y se manda al panel.
    trama_tx[0] = Data[0];
    trama_tx[1] = Data[1];
    trama_tx[2] = Data[2];
    trama_tx[3] = Data[3];
    trama_tx[4] = Data[4];
    trama_tx[5] = Data[5];
    transmitir = true;
  }
  
  if (Data[0] == 0xaa)      // Si el PC manda un byte 0xaa, es que ha leido el buffer USB.
  {
    bufferListo = false;   // por lo que se pone el buffer a 0.
  }
}

